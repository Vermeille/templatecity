/*
** main.cpp for SarkoSimulator
**
** Made by Guillaume "Vermeille" Sanchez
** Login   sanche_g <Guillaume.V.Sanchez@gmail.com>
**
** Started on  dim. 19 févr. 2012 22:06:32 CET Guillaume "Vermeille" Sanchez
** Last update mer. 29 févr. 2012 11:27:01 CET Guillaume "Vermeille" Sanchez
*/

#pragma once

extern std::map<std::type_index, float> _values_;

template<class T>
float& Parameter()
{
    return _values_[typeid(T)];
}

float& Parameter(std::type_index id);

