/*
 ** main.cpp for SarkoSimulator
 **
 ** Made by Guillaume "Vermeille" Sanchez
 ** Login   sanche_g <Guillaume.V.Sanchez@gmail.com>
 **
 ** Started on  dim. 19 févr. 2012 22:06:32 CET Guillaume "Vermeille" Sanchez
 ** Last update lun. 20 févr. 2012 18:47:17 CET Guillaume "Vermeille" Sanchez
 */

#pragma once

#include <iostream>
#include <typeinfo>
#include <typeindex>
#include <map>

#include "comparators.hpp"

template <class What, class How, class About>
struct Requires
{
    static inline bool Check()
    {
        return MustBe<How, About>::Check(Parameter<What>());
    }
};

template <class... Actions>
class Action;

template <>
class Action<>
{
    public:
        static inline bool Do()
        {
            return true;
        }
};

template<class T, class What, class Cmp, class... Actions>
class Action<Requires<What, Cmp, T>, Actions...> : public Action<Actions...>
{
    public:
        static inline bool Do()
        {
            if (Requires<What, Cmp, T>::Check())
            {
                return Action<Actions...>::Do();
            }
            else
            {
                std::cout << "Erf, dépendance non satisfaite" << std::endl;
                return false;
            }
        }
};

template<template <class, class, class> class Modulator,
    class... Actions, class Method, class Other, class What>
class Action<Modulator<What, Method, Other>, Actions...> : public Action<Actions...>
{
    public:
        static inline bool Do()
        {
            float val = Parameter<What>();
            Modulator<What, Method, Other>::Update();
            if (!Action<Actions...>::Do())
            {
                Parameter<What>() = val;
                return false;
            }
            return true;
        }
};


