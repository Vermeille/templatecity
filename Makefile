SRC=main.cpp parameter.cpp
CXXFLAGS=-Wall -Werror -Wextra -std=c++0x

all:$(SRC:.cpp=.o)
	$(CXX) $(OUTPUT_OPTION) $^ $(CXXFLAGS)
