#pragma once
#include <string>

#include <SFML/Graphics.hpp>

class Game
{
    public:
        Game(const int w, const int h, const std::string name);
        void Go();

    protected:
        virtual void Init() = 0;
        virtual void Update(const float elapsed) = 0;
        virtual void Draw(const float elapsed) = 0;

        sf::RenderWindow* win_;
        sf::Keyboard* keyb_;
};

