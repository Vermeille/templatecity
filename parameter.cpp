/*
** main.cpp for SarkoSimulator
**
** Made by Guillaume "Vermeille" Sanchez
** Login   sanche_g <Guillaume.V.Sanchez@gmail.com>
**
** Started on  dim. 19 févr. 2012 22:06:32 CET Guillaume "Vermeille" Sanchez
** Last update mer. 29 févr. 2012 11:27:06 CET Guillaume "Vermeille" Sanchez
*/

#include <iostream>
#include <typeinfo>
#include <typeindex>
#include <map>

#include "value.hpp"
#include "comparators.hpp"

std::map<std::type_index, float> _values_;

float& Parameter(std::type_index id)
{
    return _values_[id];
}

