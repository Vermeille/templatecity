/*
** main.cpp for SarkoSimulator
**
** Made by Guillaume "Vermeille" Sanchez
** Login   sanche_g <Guillaume.V.Sanchez@gmail.com>
**
** Started on  dim. 19 févr. 2012 22:06:32 CET Guillaume "Vermeille" Sanchez
** Last update mer. 29 févr. 2012 11:26:02 CET Guillaume "Vermeille" Sanchez
*/

#include <iostream>
#include <typeinfo>
#include <typeindex>
#include <map>
#include <string>
#include <memory>

#include "value.hpp"
#include "comparators.hpp"
#include "parameter.hpp"
#include "action.hpp"

#define VALUE(T) class T : public Value<T>{}
#define PARAM(T, V...) class T : public Value<T, V>{}
#define ACTION(T, V...) class T : public Action<V>{}

std::multimap<std::string, std::type_index> _names_;

VALUE(Logement);
VALUE(Budget);
PARAM(Population,
        Do<Population, Plus, Number<50>>,
        MustBe<LessEqThan, Logement>);

ACTION(BuyBuilding,
        Do<Budget, Minus, Number<1000>>,
        Do<Logement, Plus, Number<100>>,
        Requires<Budget, MoreEqThan, Number<0>>);

int main(void)
{
    Logement::Set(1000);
    Population::Set(200);
    Budget::Set(5000);
    while (true)
    {
        Population::Update();
        std::cout << Parameter<Population>()
            << " personnes pour une capacité de "
            << Parameter<Logement>() << ". Budget : "
            << Parameter<Budget>() << std::endl;
        std::cout << "Acheter des logements ?" << std::endl;
        int rep;
        std::cin >> rep;
        if (rep)
        {
            BuyBuilding::Do();
        }
    }
    std::cout << Parameter<Logement>() << std::endl;
}

