/*
** main.cpp for SarkoSimulator
**
** Made by Guillaume "Vermeille" Sanchez
** Login   sanche_g <Guillaume.V.Sanchez@gmail.com>
**
** Started on  dim. 19 févr. 2012 22:06:32 CET Guillaume "Vermeille" Sanchez
** Last update lun. 20 févr. 2012 04:31:12 CET Guillaume "Vermeille" Sanchez
*/

#pragma once

#include <iostream>
#include <typeinfo>
#include <typeindex>
#include <map>

#include "value.hpp"
#include "parameter.hpp"

template <int Nbr>
struct Number{};

template <class Comparator, class T>
struct MustBe
{
    static bool inline Check(float v)
    {
        return Comparator::Check(Parameter<T>(), v);
    }
};

template <class Comparator, int N>
struct MustBe<Comparator, Number<N>>
{
    static bool inline Check(float v)
    {
        return Comparator::Check(N, v);
    }
};

struct LessThan
{
    static bool inline Check(float v1, float v2)
    {
        return v1 > v2;
    }
};

struct MoreThan
{
    static bool inline Check(float v1, float v2)
    {
        return v1 < v2;
    }
};

struct LessEqThan
{
    static bool inline Check(float v1, float v2)
    {
        return v1 >= v2;
    }
};

struct MoreEqThan
{
    static bool inline Check(float v1, float v2)
    {
        return v1 <= v2;
    }
};

