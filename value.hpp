/*
 ** main.cpp for SarkoSimulator
 **
 ** Made by Guillaume "Vermeille" Sanchez
 ** Login   sanche_g <Guillaume.V.Sanchez@gmail.com>
 **
 ** Started on  dim. 19 févr. 2012 22:06:32 CET Guillaume "Vermeille" Sanchez
 ** Last update mer. 22 févr. 2012 16:28:49 CET Guillaume "Vermeille" Sanchez
 */

#pragma once

#include <iostream>
#include <typeinfo>
#include <typeindex>
#include <map>

#include "comparators.hpp"

template <class What, class Method, class Rate>
struct Do
{
    static inline bool Update()
    {
        Method::Modify(Parameter<What>(), Parameter<Rate>());
        return true;
    }
};

template <class What, class Method, int N>
struct Do<What, Method, Number<N>>
{
    static inline bool Update()
    {
        Method::Modify(Parameter<What>(), N);
        return true;
    }
};

struct Minus
{
    static inline void Modify(float& val, float rate)
    {
        val -= rate;
    }
};

struct Plus
{
    static inline void Modify(float& val, float rate)
    {
        val += rate;
    }
};

template <class... Actions>
class Value;

template <class T>
class Value<T>
{
    public:
        static inline bool Update()
        {
            return true;
        }

        static inline void Set(float val)
        {
            Parameter<T>() = val;
        }
};

template<class T, class Cmp, class... Actions, class Me>
class Value<Me, MustBe<Cmp, T>, Actions...> : public Value<Me, Actions...>
{
    public:
        static inline bool Update()
        {
            if (MustBe<Cmp, T>::Check(Parameter<Me>()))
            {
                return Value<Me, Actions...>::Update();
            }
            else
            {
                std::cout << "Erf, dépendance non satisfaite" << std::endl;
                return false;
            }
        }
};

template<template <class, class, class> class Action,
    class... Actions, class Me, class Method, class Other, class What>
class Value<Me, Action<What, Method, Other>, Actions...> : public Value<Me, Actions...>
{
    public:
        static inline bool Update()
        {
            float val = Parameter<Me>();
            Action<What, Method, Other>::Update();
            if (!Value<Me, Actions...>::Update())
            {
                Parameter<Me>() = val;
                return false;
            }
            return true;
        }
};


